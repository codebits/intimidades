#include <Metro.h>
#include <Servo.h>

Metro time1 = Metro(5);
Metro time2 = Metro(10); 
Metro time3 = Metro(15);
Metro timeLed1 = Metro(20);
Metro timeLed2 = Metro(25); 
Metro timeLed3 = Metro(30); 
Metro Servo1 = Metro(35);
Metro Servo2 = Metro(40); 
Metro Servo3 = Metro(45); 


Servo servoBox1;
Servo servoBox2;
Servo servoBox3;
//
int switchBoxOne = 8;
int servoBoxOne = 2;
int LedsBoxOne = 3;
//
int switchBoxTwo = 11;
int servoBoxTwo = 4;
int LedsBoxTwo = 5;
//
int switchBoxThree = 12;
int servoBoxThree = 7;
int LedsBoxThree = 6;

int pos1 = 0;
int pos2 = 0;
int pos3 = 0;
int light1 = 0;
int light2 = 0;
int light3 = 0;
int signal1 = 1;
int signal2 = 1;
int signal3 = 1;
boolean go1 = true;
boolean go2 = true;
boolean go3 = true;

void setup()
{
pinMode(switchBoxOne,INPUT);
pinMode(servoBoxOne,OUTPUT);
pinMode(switchBoxTwo,INPUT);
pinMode(servoBoxTwo,OUTPUT);
pinMode(switchBoxThree,INPUT);
pinMode(servoBoxThree,OUTPUT);
Serial.begin(9600);

}


void loop()
{

//////////////////  ONE  ///////////////////
  if(digitalRead(switchBoxOne) == 0){
    if(go1 == true) {
      time1 = Metro(800);
      timeLed1 = Metro(15);
      pos1 = 0;
      go1 = false;}  
      servoBox1.attach(servoBoxOne);
      analogWrite(LedsBoxOne,light1);
      servoBox1.write(pos1);
      if(timeLed1.check() == 1)
      {light1=light1+signal1;
      timeLed1 = Metro(15);}
      if(light1 == 255){signal1 = -1;}
      if(light1 == 50 ){signal1 = 1;}
      if(time1.check() == 1)
      {if(pos1 == 0 ){pos1 = 6;}
      else if (pos1 == 6 ){pos1 =0;}
      time1 = Metro(300);}   
  } 
  else if (digitalRead(switchBoxOne) == 1){        
            go1 = true; 
            pos1 = 0;
            Metro  Servo1 = Metro(500);
            servoBox1.write(pos1);
            if(Servo1.check() == 1){           
            servoBox1.detach();}
            analogWrite(LedsBoxOne,0);}
           
//////////////////  TWO ///////////////////           
   if(digitalRead(switchBoxTwo) == 0){
    if(go2 == true) {
      time2 = Metro(800);
      timeLed2 = Metro(15);
      pos2 = 105;
      go2 = false;}  
      servoBox2.attach(servoBoxTwo);
      analogWrite(LedsBoxTwo,light2);
      servoBox2.write(pos2);
      if(timeLed2.check() == 1)
      {light2=light2+signal2;
      timeLed2 = Metro(15);}
      if(light2 == 255){signal2 = -1;}
      if(light2 == 50 ){signal2 = 1;}
      if(time2.check() == 1)
      {if(pos2 == 105 ){pos2 = 90;}
      else if (pos2 == 90 ){pos2 = 105;}
      time2 = Metro(1500);}   
  }
  else if (digitalRead(switchBoxTwo) == 1){        
            go2 = true; 
            pos2 = 0;
            servoBox2.write(pos2);
           // delay(300);
            servoBox2.detach();
            analogWrite(LedsBoxTwo,0);}
 
//////////////////  THREE  /////////////////// 
  if(digitalRead(switchBoxThree) == 0){
    if(go3 == true) {
      time3 = Metro(800);
      timeLed3 = Metro(15);
      pos3 = 0;
      go3 = false;}  
      servoBox3.attach(servoBoxThree);
      analogWrite(LedsBoxThree,light3);
      servoBox3.write(pos3);
      if(timeLed3.check() == 1)
      {light3=light3+signal3;
      timeLed3 = Metro(15);}
      if(light3 == 255){signal3 = -1;}
      if(light3 == 50 ){signal3 = 1;}
      if(time3.check() == 1)
      {  if(pos3 == 0 ){pos3 = 6;}
         else if (pos3 == 6 ){pos3 = 0;}
         time3 = Metro(800);}   
  }
  else if (digitalRead(switchBoxThree) == 1){
            go3 = true; 
            pos3 = 0;
            servoBox3.write(pos3);
           // delay(300);
            servoBox3.detach();
            analogWrite(LedsBoxThree,0);} 
}
